package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> answer = new HashMap<>();
        int[] field = new int[adjacencyMatrix.length];
        boolean[] isUsed = new boolean[adjacencyMatrix.length];

        for (int i = 0; i < field.length; i++) {
            if (startIndex!=i) {
                field[i] = Integer.MAX_VALUE;
            }
        }
        boolean noEntries=false;

        int min = Integer.MAX_VALUE;
        int weight = 0;
        int minIndex = 0;
        for (int i = 0; i < field.length; i++) {


            for (int j = 0; j < field.length; j++) {
                if ((adjacencyMatrix[startIndex][j] != 0)
                        && ((adjacencyMatrix[startIndex][j] + weight) < field[j])
                        && (!isUsed[j])) {

                    field[j] = adjacencyMatrix[startIndex][j] + weight;
                }
            }
            for (int j = 0; j < field.length; j++) {
                if ((!isUsed[j]) && (field[j] < min)) {

                    min = field[j];
                    startIndex = j;

                }
            }
            System.out.println(min);
            weight = min;
            answer.put(startIndex, min);
            min=Integer.MAX_VALUE;

            isUsed[startIndex]=true;

        }

        return answer;


    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        int startIndex=0;
        boolean flag=false;
        int index=0;
        int answer=0;
        int[] used= new int[adjacencyMatrix.length];
        int k=0;
        int [][] buffer= new int[adjacencyMatrix.length][adjacencyMatrix.length];
        int[] previousIndexes= new int[adjacencyMatrix.length-1];
        int min= Integer.MAX_VALUE;
        boolean[] isUsed=new boolean[adjacencyMatrix.length];


        while (k!=adjacencyMatrix.length) {
            isUsed[startIndex] = true;

            for (int i = 0; i< adjacencyMatrix.length ; i++) {
                buffer[startIndex][i]=adjacencyMatrix[startIndex][i];
            }
            flag=false;
            for (int  i = 0; i < adjacencyMatrix.length; i++) {
                if ((adjacencyMatrix[startIndex][i]<min)&&(!isUsed[i])&&(adjacencyMatrix[startIndex][i]!=0)){
                    flag=true;
                    min=adjacencyMatrix[startIndex][i];
                    index=i;

                }
            }


            for (int i = 0; i < adjacencyMatrix.length ; i++) {
                for (int j = 0; j < adjacencyMatrix.length ; j++) {
                    if ((buffer[i][j]<min)&&(buffer[i][j]!=0)&&(!isUsed[j])){
                        flag=true;
                        min=buffer[i][j];
                        index=j;

                    }
                }
            }

            if (flag) {

                startIndex = index;
                answer = answer + min;
            } else {
                break;
            }
            k=k+1;


            min=Integer.MAX_VALUE;
        }


        return answer;


    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        boolean flag=false;

        int f = 0;
        int k = 0;
        int a = 0;
        int b = 0;
        int min = 0;
        int answer = 0;
        ArrayList<Integer> second = new ArrayList<>(adjacencyMatrix.length);
        for (int i = 0; i < adjacencyMatrix.length ; i++) {
            second.add(-1);

        }
        int[] nullik={-1,-1};
        ArrayList<int[]> secondd = new ArrayList<>(adjacencyMatrix.length);
        for (int i = 0; i < adjacencyMatrix.length ; i++) {
            secondd.add(i,nullik);

        }

        Deque deque = new ArrayDeque();
        min = Integer.MAX_VALUE;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if ((adjacencyMatrix[i][j] < min) && (adjacencyMatrix[i][j] != 0)) {
                    min = adjacencyMatrix[i][j];
                    a = i;
                    b = j;
                }
            }
        }
        deque.addLast(a);
        deque.addLast(b);
        answer=answer+min;

        adjacencyMatrix[a][b] = 0;
        adjacencyMatrix[b][a] = 0;
        k = k + 1;
        while (deque.size()!=adjacencyMatrix.length&&k!= adjacencyMatrix.length-1) {
            flag=false;
            // &&(!deque.contains(i)||!deque.contains(j))
            min = Integer.MAX_VALUE;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if ((adjacencyMatrix[i][j] < min) && (adjacencyMatrix[i][j] != 0)
                            &&(!deque.contains(i)||!deque.contains(j))){
                        flag=true;
                        min = adjacencyMatrix[i][j];
                        adjacencyMatrix[j][i]=0;
                        a = i;
                        b = j;
                    }
                }
            }
            k=k+1;
            // System.out.println(deque.contains(5));
            //System.out.println("daaasdsd" + adjacencyMatrix[a][b]);
            adjacencyMatrix[a][b] = 0;
            adjacencyMatrix[b][a] = 0;
            //System.out.println(a);

            if (min==Integer.MAX_VALUE){

            } else {
                if (deque.contains(a) && deque.contains(b) && flag) {
                    //  System.out.println("Занято");
                } else {
                    if (!deque.contains(a) && !deque.contains(b)
                            && (Arrays.equals(secondd.get(b), new int[]{-1, -1})) && (Arrays.equals(secondd.get(a), new int[]{-1, -1}))
                            && flag == true) {
                        //  System.out.println(" rtrtt  "+second.size());
                        int[] x = {a, b};
                        secondd.add(a, x);

                        int[] y = {b, a};
                        secondd.add(b, y);

                    } else {
                        if (deque.contains(a) && (Arrays.equals(secondd.get(b), new int[]{-1, -1})) && !deque.contains(b) && flag == true) {
                            deque.add(b);

                        } else {
                            if (deque.contains(b) && (Arrays.equals(secondd.get(a), new int[]{-1, -1})) && !deque.contains(a) && flag == true) {
                                deque.add(a);

                            } else {
                                if (deque.contains(a) && (!Arrays.equals(secondd.get(b), new int[]{-1, -1})) && !deque.contains(b) && flag == true) {



                                    for (int i = 0; i < 2; i++) {
                                        deque.add(secondd.get(b)[i]);
                                    }
                                    secondd.set(b, new int[]{-1, -1});

                                } else {
                                    if (!deque.contains(a) && (Arrays.equals(secondd.get(a), new int[]{-1, -1})) && (!Arrays.equals(secondd.get(b), new int[]{-1, -1}))
                                            && (!deque.contains(b)) && flag == true) {
                                        secondd.set(a, new int[]{a, b});
                                    } else {
                                        if (!deque.contains(b) && (Arrays.equals(secondd.get(b), new int[]{-1, -1}))
                                                && (!Arrays.equals(secondd.get(a), new int[]{-1, -1}))
                                                && (!deque.contains(a)) && flag == true) {
                                            secondd.set(b, new int[]{b, a});
                                        }

                                    }
                                }
                            }

                        }
                    }

                }
            }



            answer = answer + min;

            //System.out.println(k);


        }
        return answer;
    }
}
